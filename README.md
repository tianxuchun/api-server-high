# openresty 数据接收模块 #

简介

openresty 数据接收模块实现了 lua 语言处理 http 的 post 请求数据与 kafka 之间的通路。是为了应对接收客户活跃高并发场景而开发的基于openresty 的 nginx 模块，通过动态继承luajit和代码逻辑的优化极大的提高并发处理能力和速度。

tutorial

openresty 数据接收模块主要的功能可以拆分成3块：

* 读取post数据

```
#!lua

ngx.req.read_body()
data = ngx.req.get_body_data()

```

* 解析密文

前期准备：

编译解析代码，这部分属于订制的协议，不考虑开源

gcc  -fpic -shared du_data_kit.c -o libd1.so  -lz 

拷贝ld1.so到/usr/lib64

cp libd1.so /usr/lib64

编译luajit的callback代码，编译时需要同时指定引用源码路径和引用so用到的源码路径

gcc -fPIC packet_wrapper.c -o protocol.so -shared -I/usr/local/openresty/luajit/include/luajit-2.1/ -I/srv/app/ngx_dudna_module -ld1

* kafka操作，需要：

    1) 集成 openresty 外部 lua 模块

首先准备代码(可以直接使用工程中的lua-resty-kafka)


```
#!shell

git clone https://github.com/doujiang24/lua-resty-kafka.git
```



    2) 编辑配置文件，在作用域内增加引用


```
#!conf

http {
    lua_package_path "/usr/local/openresty/nginx/lua-resty-kafka/lib/?.lua;;";
    server {
        listen 8080;
        location / {
            default_type text/html;
            content_by_lua_file /usr/local/openresty/nginx/daa.lua;
        }
    }
}
```

    3） 重启openresty加载配置和系统


相关技术文档：

http://www.lua.org/manual/5.1/manual.html
https://github.com/doujiang24/lua-resty-kafka
https://github.com/openresty/lua-nginx-module
https://github.com/calio/form-input-nginx-module
https://github.com/openresty/lua-resty-redis#methods