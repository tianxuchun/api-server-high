worker_processes  4;
error_log logs/error.log;
events {
    worker_connections 10240;
}
http {
    lua_package_path "/usr/local/openresty/nginx/lua-resty-kafka/lib/?.lua;;";
    server {
        listen 8080;
        location / {
            default_type text/html;
            #content_by_lua 'ngx.say("<p>hello, world</p>")';
            content_by_lua_file /usr/local/openresty/nginx/test.lua;
        }
    }
}
