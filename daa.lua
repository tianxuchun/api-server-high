local protocol = require "protocol"
local cjson = require "cjson"
local client = require "resty.kafka.client"
local producer = require "resty.kafka.producer"
----init func
local welcome = 'Hello World and file and process'


local broker_list = {
    { host = "121.42.167.23", port = 9092 , socket_timeout = 10,keepalive_timeout = 10,keepalive_size = 10 },
}

print("111")
ngx.say(welcome)

----read payload
ngx.req.read_body()
data = ngx.req.get_body_data()

----check payload
ngx.say(data)
ngx.say(string.len(data))

----use c extention work
text = protocol.parse(data)
key = "test"
ngx.say('processed')

----send queue
local bp = producer:new(broker_list, { producer_type = "async" })

local ok, err = bp:send("test", key, text)
if not ok then
    ngx.say("send err:", err)
    return
end
ngx.say("send success, ok:", ok)

ngx.say(text)
ngx.say(string.len(text))
--         local args, err = ngx.req.get_post_args()
--         if not args then
--             ngx.say("failed to get post args: ", err)
--             return
--         end
--         for key, val in pairs(args) do
--             if type(val) == "table" then
--                 ngx.say(key, ": ", table.concat(val, ", "))
--             else
--                 ngx.say(key, ": ", val)
--             end
--         end
