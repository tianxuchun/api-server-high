#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <string.h>

#include "du_data_kit.h"

#define MODERN_SECRET_1_0 "f!e90`-)45&pjqli~jl#etu98v2u@7v6.a1dv3o5$%^&+="
#define UNCOMPRESS_MAX_LEN 20000
#define INT_LEN 10

/****************************************************************************
 *name:      itoa
 *parameter: int , char *
 *return:    char *
 *descrption:
 *           asist function keep return value type consistent
 *****************************************************************************/
char * itoa(int num, char *itoa_str)
{
  sprintf(itoa_str, "%ld", num);
  return itoa_str;
}

/****************************************************************************
 *name:      wrap_fact
 *parameter: lua_State
 *return:    int
 *descrption:
 *           wrap service logic as lua module ,and called by lua
 *****************************************************************************/
int wrap_fact(lua_State* L)
{
  size_t rawLen;
  int result;
  int secretLen = sizeof(MODERN_SECRET_1_0) - 1;
  u_char unRequest_body[UNCOMPRESS_MAX_LEN];
  size_t unDataLen = UNCOMPRESS_MAX_LEN;
  u_char *raw = NULL;
  const char *craw = lua_tolstring (L, 1, &rawLen);
  char ret[INT_LEN];

  printf("wrap_fact:\n");
  printf("self %p\n", L);
  memset(unRequest_body,0,UNCOMPRESS_MAX_LEN);
  memset(ret,0,INT_LEN);
  raw = (u_char *)craw;

  printf("self %d,args %s\n", rawLen, raw);
  result = ddk_unwrap_data_modern(unRequest_body, &unDataLen, raw, rawLen, MODERN_SECRET_1_0, secretLen);
  if( result != 0)
  {
    printf("fail,%s","false2");
    itoa(result,ret);
    lua_pushstring(L,ret);
  }
  else
  {
    printf("ok,%s",unRequest_body);
    lua_pushlstring(L,unRequest_body,unDataLen);
  }
  //printf("done\n");
  //printf("done %s\n",unRequest_body);
  //result = fact(n,a);
  return 1;
}

/****************************************************************************
 *name:      mylibs
 *parameter: 
 *return:    
 *descrption:
 *          luaL_Reg结构体的第一个字段为字符串，在注册时用于通知Lua该函数的名字。
 *          第一个字段为C函数指针。
 *          结构体数组中的最后一个元素的两个字段均为NULL，用于提示Lua注册函数已经到达数组的末尾。
 *****************************************************************************/
static luaL_Reg mylibs[] = { 
    {"parse", wrap_fact},
    {NULL, NULL} 
}; 

/****************************************************************************
 *name:      luaopen_protocol
 *parameter: lua_State*
 *return:    int
 *descrption:
 *          该C库的唯一入口函数。其函数签名等同于上面的注册函数。见如下几点说明：
 *          1. 我们可以将该函数简单的理解为模块的工厂函数。
 *          2. 其函数名必须为luaopen_xxx，其中xxx表示library名称。Lua代码require "xxx"需要与之对应。
 *          3. 在luaL_register的调用中，其第一个字符串参数为模块名"xxx"，第二个参数为待注册函数的数组。
 *          4. 需要强调的是，所有需要用到"xxx"的代码，不论C还是Lua，都必须保持一致，这是Lua的约定，
 *             否则将无法调用。
 *****************************************************************************/
int luaopen_protocol(lua_State* L) 
{
    const char* libName = "protocol";
    luaL_register(L,libName,mylibs);
    return 1;
}
