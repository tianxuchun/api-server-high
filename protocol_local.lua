local protocol = require "protocol"

function getFile(file_name)
  local f = assert(io.open(file_name, 'r'))
  local string = f:read("*all")
  f:close()
  return string
end
a=getFile("active_package.bin")
b=protocol.parse(a)
print(b)
